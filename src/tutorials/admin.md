---
title: The admin website
---

The same codebase that is used to deploy an instance of hub20 can also
be used to deploy the admin website. While not mandatory, it can
certainly help hub operators to execute common tasks and to visualize
the state of the application.

If you are deploying Hub20 with our docker-compose file, the admin
site will already be available at
`https://your-instance-domain/admin`. If you are deploying manually,
the only thing you need to change/add compared with the [API deployment](./deployment.md):

 - The `HUB20_URLCONF_MODULE` environment variable needs to be set to `hub20.admin.urls`
 - A new service needs to be defined to run another process of `uvicorn hub20.api.asgi:application`
 - Your web server proxy (nginx/apache/caddy/etc) needs to have another entry to this web server


If you set things up correctly, you can navigate to the admin URL and
you will be greeted by the login screen.

![Admin Login](/assets/images/tutorials/admin_login_screen.png)

Login with the credentials you used setting the "superuser" account,
and you will have access to the admin site.
