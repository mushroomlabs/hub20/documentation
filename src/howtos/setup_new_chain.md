---
title: How to connect to Ethereum-compatible blockchains
---

You can use Hub20 to interact with any EVM-compatible blockchain that
has nodes that follows its JSON-RPC protocol - e.g, Binance Smart
Chain, xDAI, Polygon/Matic and also the Layer-2 rollups such as
Arbitrum and Optimism.


### Loading all chains from chainlist.org


If you just want to get all information from the most popular
blockchains, you can start by loading the list compiled by
[Chainlist](https://chainlist.org). The `load_chain_list` command is provided.

=== "Docker"
    ``` bash
    docker-compose exec web django-admin load_chain_list
    ```
=== "Direct install"
    ``` bash
    django-admin load_chain_list
    ```


### Loading information about specific/custom blockchains


To add a new chain, you can use the `register_blockchain` command:

=== "Docker"
    ``` bash
    docker-compose exec web django-admin register_blockchain --id <chain_id> --name <name> (--provider-url <url_of_web3_provider>)
    ```
=== "Direct install"
    ``` bash
    django-admin register_blockchain --id <chain_id> --name <name> (--provider-url <url_of_web3_provider>)
    ```


## Managing Blockchains and providers through the admin

To add more information about the chains or the nodes that you want to
use to connect to these blockchains, you will need to use the admin
site.

#### The main admin view

The "Ethereum Blockchains" section on the admin main dashboard is
where you can manage anything related to connecting to EVM-compatible
blockchains.

  ![Ethereum Blockchains Section on Admin](/assets/images/admin/section_ethereum_blockchains.png)

#### Add a new chain to the database

By clicking on the 'add' link on the chain section, you will be
presented with a form where you can provide the data about the chain.

If you are adding a blockchain that is already public and used by
other people, you *should* use a name that is most recognizable. Chain
IDs are *MUST* be unique. The `short name` attribute is also meant to
be an unique identifier, but no software still uses or enforces it at
any level. If you are not sure of what information to provide here,
you can refer to [Chainlist](https://chainlist.org).

You might also be interested in providing some *metadata*, i.e, if you
want to indicate that this chain is a test network, or if it is a
sidechain that connects to "main" chain (like Gnosis/xDAI), or if it
is a *roll-up* (like Optimism/Arbitrum)

  ![Add new chain on Admin](/assets/images/admin/form_add_chain.png)


#### Add a new provider

To add a new provider, simply indicate the URL of the node. If your
node requires some form of authentication, you can put the credentials
in the `<username>:<password>@<host>` format.

  ![Add new web3 provider on Admin](/assets/images/admin/form_add_web3provider.png)
