---
title: Payment Networks
hide:
  - toc
---

Hub20 started its development as a gateway for
[Ethereum](https://ethereum.org) and [Raiden](https://raiden.network)
and to allow its users to have a common mechanism to accept payments
and make value transfers. Starting with version 0.5.0, this concept
has been extended so that a Hub20 instance can be integrated with
any *programmable* network:

 - [Ethereum and Ethereum-compatible blockchains](./ethereum_blockchains.md)
 - [Raiden](./raiden.md)
 - "Layer-2" systems that enable off-chain transfers for
   cryptocurrencies (e.g, [Loopring](https://loopring.io),
   [ZkSync](https://zksync.com))
 - Other Blockchains (e.g, Bitcoin, Monero)
 - Centralized Exchanges with public APIs (e.g,
   [Coinbase](https://coinbase.com), [Uphold](https://uphold.com))
 - "Traditional" online Payment Gateways (Paypal, Venmo)

Naturally, the idea of "programmable money" is built-in into
cryptocurrency and blockchains, but even initiatives from traditional
financial institutions to modernize payments could lead to
integrations with Hub20. With the current architecture, it would be
possible to use Hub20 to integrate with banking networks (e.g, SEPA in
Europe, PIX in Brazil, India's NPCI or the up-coming
[FedNow](https://www.frbservices.org/financial-services/fednow) from
the USA) to track payments and initiate payouts in the appropriate
currency.

## Verifying the networks the hub is connected to

The endpoint `/networks` will list all the *active* networks in the
hub. This will show the id of the network (an internal identifier),
its *type*, its name and a basic description. The
`/networks/<network_id>` endpoint should give you more information,
specific to the network.
