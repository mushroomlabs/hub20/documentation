copyright: © MushroomLabs 2020

docs_dir: src
markdown_extensions:
  - admonition
  - attr_list
  - meta
  - pymdownx.highlight
  - pymdownx.tabbed
  - pymdownx.superfences
  - footnotes

nav:
  - Home: index.md
  - Tutorials:
      - tutorials/index.md
      - A Quick Look at Hub20: tutorials/overview.md
      - Generate secrets for managed wallets: tutorials/wallet.md
      - Deploying Hub20: tutorials/deployment.md
      - Deploying the admin site: tutorials/admin.md
      - Setup user accounts: tutorials/user_accounts.md
      - Funding wallets: tutorials/funding.md
      - Manage Raiden Nodes: tutorials/raiden.md
      - Receive Payments: tutorials/payment_gateway.md

  - Topic Guides:
      - topics/index.md
      - Payment Networks:
          - topics/payment_networks/index.md
          - Ethereum (and compatible) Blockchains: topics/payment_networks/ethereum_blockchains.md
          - Raiden: topics/payment_networks/raiden.md
      - Tokens: topics/tokens.md
      - Wallets and External Accounts: topics/wallets.md
      - Payments: topics/payment_gateway.md
      - Withdrawals and Payouts: topics/withdrawals.md
      - Accounting: topics/accounting.md

  - Reference:
      - reference/index.md
      - Environment Variables: reference/configuration.md
      - Wallet Engines: reference/wallets.md
      - API: reference/api.md
      - CLI: reference/cli.md

  - HOWTOs:
      - howtos/index.md
      - Create Admin Account: howtos/create_admin_account.md
      - Connect to Ethereum-compatible Blockchains: howtos/setup_new_chain.md
      - Connect to a Raiden node: howtos/raiden.md
      - Select tokens: howtos/tokens.md
      - Deployment: howtos/deployment.md
      - Use HD Wallets: howtos/hd_wallets.md
      - Customize Checkout20 Widget: howtos/checkout20.md
      - Troubleshooting: howtos/troubleshooting.md

  - User manual:
      - manual/index.md
      - Accounts:
          - manual/accounts/register.md
          - manual/accounts/profile.md
      - Wallet:
          - View Balances: manual/wallet/view.md
          - Receive Deposits: manual/wallet/add_funds.md
          - Send/Withdraw Funds: manual/wallet/withdraw.md
      - History: manual/history.md
      - Portfolio Management: manual/token_management.md
      - Create Merchant Store: manual/merchant_stores.md

  - FAQ: faq.md

plugins:
  - search
  - render_swagger
  - macros:
      include_dir: snippets

repo_name: mushroomlabs/hub20
repo_url: https://github.com/mushroomlabs/hub20

site_url: https://docs.hub20.io
site_name: Hub20

theme:
  custom_dir: overrides
  name: material
  font:
    text: Ubuntu
    code: Ubuntu Mono
  features:
    - navigation.tabs
    - navigation.indexes
  language: en
  palette:
    primary: indigo
    accent: indigo
